#pragma once

#include <iostream>
#include <vector>
#include <ctime>
#include <sstream>

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>

// Game Engine class/wrapper
class Game
{
	private:
		//
		sf::RenderWindow* window;
		sf::VideoMode videoMode;
		sf::Event ev;

		// mouse positions
		sf::Vector2i mousePosWindow;
		sf::Vector2f mousePosView;

		// Resources
		sf::Font font;

		// Text
		sf::Text uiText;

		// game logic
		bool endGame;
		unsigned points;
		int health;
		float enemySpawnTimer;
		float enemySpawnTimerMax;
		int maxEnemies;
		bool mouseHeld;

		// Game objects
		std::vector<sf::RectangleShape> enemies;
		sf::RectangleShape enemy;

		// private functions
		void initVariables();
		void initWindow();
		void initFonts();
		void initText();
		void initEnemies();

	public:
		Game();
		virtual ~Game();

		// Accessors
		const bool running() const;
		const bool getEndGame() const;

		// Functions
		void spawnEnemy();

		void pollEvents();
		void updateMousePositions();
		void updateText();
		void updateEnemies();
		void update();

		void renderText(sf::RenderTarget& target);
		void renderEnemies(sf::RenderTarget& target);
		void render();

};
