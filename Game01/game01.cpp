#include <iostream>
#include "Game.h"

int main()
{
	std::srand(static_cast<unsigned>(time(NULL)));

	// Init engine
	Game game;

	while (game.running() && !game.getEndGame())
	{
		// update
		game.update();

		// game render
		game.render();

	}

	return 0;
}